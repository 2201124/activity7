
public class Solutions {
    public static void main(String[]args){
        Solutions s = new Solutions();
        System.out.println(s.BlackSquare("0 0 0 0","0000000"));
    }

    /**
     * @param x matrix to be processed
     * @return returns "YES" if the matrix is equilibrium otherwise returns "NO"
     */
    public String YoungPhysicist(int x[][]){
        int[][] mat = x;
        int[] sums = new int[3];
        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < 3; j++) {
                sums[j] += mat[i][j];
            }
        }

        if (sums[0] == 0 && sums[1] == 0 && sums[2] == 0){
            return "YES";
        }
        else{

            return "NO";
        }
    }


    /**
     * @param word String to be processed
     * @return returns the input string with a,e,i,o,u,y replaced by '.'
     */
    public String StringTask(String word){
        word = word.toLowerCase();
        word = word.replaceAll("[aeiouy]","");
        StringBuilder stringBuilder = new StringBuilder(word);
        for(int i=0; i<stringBuilder.length(); i+=2){
            stringBuilder.insert(i,'.');
        }
        return String.valueOf(stringBuilder);
    }


    /**
     * @param word String to be processed
     * @return Returns all uppercase word if the number of uppercase letter is greater than lowercase
     * otherwise returns lowercase word
     */
    public String Word(String word){
        String lowCase = word.toLowerCase();
        String upCase = word.toUpperCase();
        int lowerCount = 0;
        for(int i = 0; i<word.length(); i++)
        {
            if(word.toCharArray()[i] == lowCase.toCharArray()[i])
                lowerCount++;
        }
        int u = 0;
        for(int i = 0;i < word.length(); i++)
        {
            if(word.toCharArray()[i] == upCase.toCharArray()[i])
                u++;
        }
        if(lowerCount>u || lowerCount == u){
            return word.toLowerCase();
        }

        else{
            return word.toUpperCase();
        }

    }

    /**
     * @param watermelon The number of watermelons.
     * @return Returns YES or NO
     */
    public String Watermelon (int watermelon)
    {
        if (watermelon > 2 && watermelon % 2 == 0)
        {
            return "YES";
        } else
        {
            return "NO";
        }
    }

    /**
     * @param word Input string
     * @return Returns YES if the string contains 0 or 1, otherwise returns NO
     */
    public String Football(String word) {

        if (word.contains("0000000") || word.contains("1111111")) {
            return "YES";
        } else {
            return "NO";
        }
    }

    /**
     *
     * @param y Integer to be check if it is a sum of 2050
     * @return returns the sum if the input is a sum of 2050 otherwise returns -1
     */

    public int sumof2050(long y){
        if(y%2050==0){
            long z =y/2050;
            int sum = 0;
            while (z!=0){
                sum+= z%10;
                z/=10;
            }
            return sum;
        }else{
            return -1;
        }
    }

    /**
     *
     * @param n Determines the length of the string
     * @param a Length of substrinig
     * @param b Number of distinct letters
     * @return Returns the string that is length of n and has b distinct letters
     */
    public String ConstructtheString(int n, int a,  int b){
        String s ="";
        for(int i =0, j=0; i<n; i++,j++){
            s+= (char)((j%b)+'a');
        }
        return s;
    }

    /**
     * @param a The weight of Big bro
     * @param b The weight of Step bro
     * @return returns the year in which big bro surpasses the weight of step bro
     */
    public int BigBroAndStepBro(int a, int b){
        int year = 0;

        while(a <= b) {
            a *= 3;
            b *= 2;
            year++;
        }
        return year;
    }

    /**
     * @param num The number to be processed
     * @return Returns the sum of each digits in num
     */
    public int SumOfDigits(int num){
        int digit;
        int sum = 0;
        while(num > 0)
        {
            digit = num % 10;
            sum = sum + digit;
            num = num / 10;
        }
        return sum;
    }

    /**
     * @param word String to be processed
     * @return Returns the inputted word with the first letter capitalized
     */
    public String Capslock(String word){

        String firstLetter = word.substring(0, 1);
        String others = word.substring(1, word.length());

        firstLetter = firstLetter.toUpperCase();

        word = firstLetter + others;
        return word;
    }

    /**
     * @param list the scores of the participants
     * @param y The rank of the participant who's score will be the minimum requirement to get to the next round
     * @return returns the number of participants who can enter the next round
     */
    public int NextRound(int list[],int y){
        int z = 0;
        for (int i = 0; i < list.length; i++)
            if (list[i] > 0 && list[i] >= list[y-1])
                z++;

        return z;
    }

    /**
     * @param input 4 space separated integer
     * @param input2 String to be processed
     * @return  the total number of calories wasted
     */
    public int BlackSquare(String input,String input2){

        int[] xSubs=new int[4];
        int result = 0;

        String[] ySubs = input.split(" ");

        for (int i=0;i<4;i++){
            xSubs[i]=Integer.parseInt(ySubs[i]);
        }

        input = input2;
        for (int i=0;i<input.length();i++){
            result += xSubs[ Character.getNumericValue( input.charAt(i)) -1 ];
        }

        return result;
    }

}
